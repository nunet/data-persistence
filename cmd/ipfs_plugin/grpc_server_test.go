package main

import (
	"context"
	"net"
	"testing"

	pb "gitlab.com/nunet/data-persistence/api/gen/ipfs_plugin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	pb.RegisterIPFSPluginServer(s, &serverIPFS{})
	go func() {
		if err := s.Serve(lis); err != nil {
			panic(err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func TestStore(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()

	client := pb.NewIPFSPluginClient(conn)
	resp, err := client.Store(ctx, &pb.StoreRequest{ContainerId: "test"})
	if err != nil {
		t.Fatalf("Store failed: %v", err)
	}

	if resp.CID != "Qm1234e2idfjs" {
		t.Fatalf("Unexpected response: %v", resp)
	}
}
