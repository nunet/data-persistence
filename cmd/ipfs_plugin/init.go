package main

import (
	"gitlab.com/nunet/data-persistence/internal/logger"
)

var zlog *logger.Logger

func init() {
	zlog = logger.NewZapLogger("grpc_server")
}
