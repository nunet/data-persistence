package main

import (
	"context"
	"flag"
	"fmt"
	"net"

	pb "gitlab.com/nunet/data-persistence/api/gen/ipfs_plugin"
	"gitlab.com/nunet/data-persistence/pkg/ipfs"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	port = flag.Int("port", 31001, "The server port")
	addr = flag.String("addr", "localhost", "The server address")
)

const (
	kuboSwarmPort   = "4001"
	kuboAPIPort     = "5001"
	kuboGatewayPort = "8080"
)

type serverIPFS struct {
	pb.UnimplementedIPFSServer
}

// StoreOutput is the RPC function which stores a piece of data on IPFS (through Kubo node) and returns its CID
func (s *serverIPFS) StoreOutput(ctx context.Context, req *pb.StoreOutputRequest) (*pb.StoreOutputResponse, error) {
	zlog.Sugar().Debugf("Receivd request. INFO: %v\n%v", ctx, req)
	cid, err := ipfs.StoreOutput(ctx, req.ContainerId)
	if err != nil {
		// TODO: return different gRPC code errors depending on the error coming from StoreOutput
		return &pb.StoreOutputResponse{},
			status.Errorf(codes.Unknown, fmt.Sprintf("Failed to store output on IPFS, error: %v", err))
	}

	response := &pb.StoreOutputResponse{
		CID: cid,
	}
	return response, nil
}

// PinByCID is the RPC function which downloads and pin a piece of data based on the given CID
func (s *serverIPFS) PinByCID(ctx context.Context, req *pb.PinByCIDRequest) (*pb.PinByCIDResponse, error) {
	zlog.Sugar().Debugf("Receivd request. INFO: %v\n%v", ctx, req)
	if err := ipfs.PinByCid(ctx, req.GetCID()); err != nil {
		zlog.Sugar().Error(err)
		// TODO: return different gRPC code errors depending on the error coming from StoreOutput
		return &pb.PinByCIDResponse{},
			status.Errorf(codes.Unknown,
				fmt.Sprintf("Failed to pin data based on its CID, error: %v", err))
	}
	return &pb.PinByCIDResponse{}, nil
}

func main() {
	flag.Parse()
	ctx := context.Background()

	listen, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *addr, *port))
	if err != nil {
		zlog.Sugar().Errorf("Failed to listen: %v", err)
		return
	}

	// TODO: for now, running kubo node by default. This might change in the future
	zlog.Sugar().Infof("Starting Kubo node with rpc API ports of: %v, swarm ports: %v\n", kuboAPIPort, kuboSwarmPort)
	kubo, err := ipfs.RunKuboNode(ctx, kuboSwarmPort, kuboGatewayPort, kuboAPIPort, false)
	if err != nil {
		zlog.Sugar().Errorf("Couldn't Run Kubo node, Error: %v", err)
		return
	}
	// TODO: defer kill kubo node

	s := grpc.NewServer()
	pb.RegisterIPFSServer(s, &serverIPFS{})

	zlog.Sugar().Infof("Server is running on  %s:%d...\n", *addr, *port)
	if err := s.Serve(listen); err != nil {
		err := kubo.StopAndRemoveContainer(ctx)
		if err != nil {
			zlog.Sugar().Error(err)
		}
		zlog.Sugar().Errorf("Failed to serve: %v", err)
		return
	}

}
