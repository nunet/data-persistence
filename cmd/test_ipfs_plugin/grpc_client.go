package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"time"

	"gitlab.com/nunet/data-persistence/api/gen/ipfs_plugin"
	"google.golang.org/grpc"
)

var (
	port        = flag.Int("port", 31001, "The server port")
	addr        = flag.String("addr", "localhost", "The server address")
	containerID = flag.String("id", "", "Container ID param")
)

func main() {
	// Set up a connection to the server.
	flag.Parse()
	log.Printf("Calling %s:%d", *addr, *port)
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", *addr, *port), grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := ipfs_plugin.NewIPFSClient(conn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.StoreOutput(ctx, &ipfs_plugin.StoreOutputRequest{ContainerId: *containerID})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Received CID: %s", r.GetCID())
}
