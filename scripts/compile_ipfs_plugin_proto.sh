dir=api/gen/

if [ ! -d "$dir" ]; then
    mkdir api/gen/
fi

protoc --go_out="$dir" --go-grpc_out="$dir" api/data-persistence-api-spec/ipfs_plugin.proto
