package docker

import (
	"bytes"
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
)

// GetContainerLogs retrieves the standard output logs of a specified container
// identified by its containerID. The Docker-specific headers are removed, and
// only the actual content of the logs is returned as a byte slice.
func GetContainerLogs(ctx context.Context, containerID string) ([]byte, error) {
	dc, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("Unable to create Docker client: %w", err)
	}

	options := types.ContainerLogsOptions{ShowStdout: true}
	logs, err := dc.ContainerLogs(ctx, containerID, options)
	if err != nil {
		return nil, fmt.Errorf("Unable to get logs: %w", err)
	}
	defer logs.Close()

	var stdout bytes.Buffer
	_, err = stdcopy.StdCopy(&stdout, nil, logs)
	if err != nil {
		return nil, fmt.Errorf("Error while copying logs: %w", err)
	}

	return stdout.Bytes(), nil
}
