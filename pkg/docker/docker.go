package docker

import (
	"context"
	"fmt"
	"io"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

// StopAndRemoveContainerByID stops and removes a Docker container if
// exists.
func StopAndRemoveContainerByID(ctx context.Context, dc *client.Client, id string) error {
	if id != "" {
		container, err := dc.ContainerInspect(ctx, id)
		if err != nil {
			return fmt.Errorf("Unable to inspect container with ID: %v, Error: %w", id, err)
		}

		if container.State.Running {
			if err := dc.ContainerStop(ctx, id, nil); err != nil {
				return fmt.Errorf(
					"Unable to stop container with ID: %v, Error: %w",
					id, err)
			}
		}

		if err := dc.ContainerRemove(ctx, id, types.ContainerRemoveOptions{Force: true}); err != nil {
			return fmt.Errorf("Unable to remove container: %v, Error: %w", id, err)
		}
	}
	return fmt.Errorf("Invalid container ID: empty")
}

// getContainerIDIfExists returns the Docker container ID based on its name
func getContainerIDIfExists(ctx context.Context, name string, dc *client.Client) (string, error) {
	containers, err := dc.ContainerList(ctx, types.ContainerListOptions{
		Filters: filters.NewArgs(filters.Arg("name", name)),
		All:     true,
	})
	if err != nil {
		return "", fmt.Errorf(
			"Unable to get list of containers: %v, Error: %w",
			name, err)
	}

	if len(containers) == 0 {
		return "", nil
	}

	return containers[0].ID, nil
}

// PullImageWrapper is a wrapper around Docker SDK's function with same name.
// This function is copied from the docker package.
func PullImageWrapper(ctx context.Context, imageName string, dc *client.Client) error {
	// TODO: We should rename the docker package to deployment package
	// and create a new docker package.
	// OR put this image in utils/utils.go
	out, err := dc.ImagePull(ctx, imageName, types.ImagePullOptions{})
	if err != nil {
		return err
	}

	defer out.Close()
	io.Copy(os.Stdout, out)
	return nil
}
