package ipfs

import (
	"context"
	"testing"

	"github.com/docker/docker/client"
	"github.com/stretchr/testify/suite"
)

type RunKuboNodeTestSuite struct {
	suite.Suite
	dc   *client.Client
	ctx  context.Context
	kubo *KuboNode
}

func (s *RunKuboNodeTestSuite) SetupSuite() {
	s.ctx = context.Background()

	var err error
	s.dc, err = client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	s.Require().NoError(err)
}

func (s *RunKuboNodeTestSuite) TearDownSuite() {
	// Teardown logic to stop and remove the container after test execution
	err := s.kubo.StopAndRemoveContainer(s.ctx)
	s.Require().NoError(err)
}

// TestRunKuboNode tests only the deployment of the Kubo Docker image
func (s *RunKuboNodeTestSuite) TestRunKuboNode() {
	kubo, err := RunKuboNode(s.ctx, "0", "0", "5001", true)
	s.Require().NoError(err)
	s.kubo = kubo

	_, err = NewKuboClient()
	s.Require().NoError(err)
}

func TestRunKuboNodeSuite(t *testing.T) {
	suite.Run(t, new(RunKuboNodeTestSuite))
}
