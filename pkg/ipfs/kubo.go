package ipfs

import (
	"context"
	"fmt"
	"path/filepath"
	"sync"

	"gitlab.com/nunet/data-persistence/pkg/docker"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/ipfs/kubo/client/rpc"
	ma "github.com/multiformats/go-multiaddr"
)

const (
	allInterfacesIP = "0.0.0.0"
	loopbackIP      = "127.0.0.1"

	kuboDockerImg = "ipfs/kubo:latest"
)

type KuboNode struct {
	cli *rpc.HttpApi
	dc  *client.Client

	containerID string

	swarmPort   string
	gatewayPort string
	rpcAPIPort  string
}

var (
	kubo *KuboNode
	mu   sync.Mutex
)

// NewKuboClient creates a client connected with the Kubo gRPC client running
// on the port passed as param
func NewKuboClient() (*rpc.HttpApi, error) {
	mu.Lock()
	defer mu.Unlock()

	if kubo.cli != nil {
		return kubo.cli, nil
	}

	if kubo.rpcAPIPort == "" {
		return nil, fmt.Errorf("the API port was not assigned when starting Kubo node")
	}

	m, err := ma.NewMultiaddr(fmt.Sprintf("/ip4/%v/tcp/%v", loopbackIP, kubo.rpcAPIPort))
	if err != nil {
		return nil, fmt.Errorf("Couldn't build MultiAddr, Error: %w", err)
	}

	httpAPI, err := rpc.NewApi(m)
	if err != nil {
		return nil, fmt.Errorf(
			"Couldn't create client for Kubo RPC HTTP API, Error: %w", err,
		)
	}

	kubo.cli = httpAPI
	return kubo.cli, nil
}

// RunKuboNode starts Kubo node (docker container) through its official docker image
func RunKuboNode(ctx context.Context, swarmPort, gatewayPort, rpcAPIPort string,
	test bool) (*KuboNode, error) {
	// Temporary Note:
	// The following is the same as the command:
	// docker run -d --name ipfs_host -e IPFS_PROFILE=server
	// -p 4001:4001 -p 4001:4001/udp -p 127.0.0.1:8080:8080 -p 127.0.0.1:5001:5001 ipfs/kubo:latest
	dc, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("Unable to initialize Docker client for Kubo node, Error %w", err)
	}

	err = docker.PullImageWrapper(ctx, kuboDockerImg, dc)
	if err != nil {
		return nil, fmt.Errorf("Couldn't pull Kubo docker image, Error: %w", err)
	}

	natSwarmPortTCP := fmt.Sprintf("%v/tcp", swarmPort)
	natSwarmPortUDP := fmt.Sprintf("%v/udp", swarmPort)
	natGatewayPort := fmt.Sprintf("%v/tcp", gatewayPort)
	natAPIPort := fmt.Sprintf("%v/tcp", rpcAPIPort)

	var portBindings nat.PortMap
	var containerMount []mount.Mount
	var env []string

	env = append(env, "IPFS_PROFILE=server")

	if test {
		testConfigFile, err := filepath.Abs("./kubo_test_config.sh")
		if err != nil {
			return nil, fmt.Errorf("Error converting to absolute path: %w", err)
		}

		containerMount = []mount.Mount{
			{
				Type:   mount.TypeBind,
				Source: testConfigFile,
				Target: "/container-init.d/kubo_test_config.sh",
			},
		}
	} else {
		containerMount = []mount.Mount{}
	}

	portBindings = nat.PortMap{
		nat.Port(natSwarmPortTCP): []nat.PortBinding{
			{HostIP: allInterfacesIP, HostPort: swarmPort},
		},
		nat.Port(natSwarmPortUDP): []nat.PortBinding{
			{HostIP: allInterfacesIP, HostPort: swarmPort},
		},
		nat.Port(natGatewayPort): []nat.PortBinding{
			{HostIP: loopbackIP, HostPort: gatewayPort},
		},
		nat.Port(natAPIPort): []nat.PortBinding{
			{HostIP: loopbackIP, HostPort: rpcAPIPort},
		},
	}

	config := &container.Config{
		Image: kuboDockerImg,
		Env:   env,
	}

	hostConfig := &container.HostConfig{
		PortBindings: portBindings,
		Mounts:       containerMount,
	}

	resp, err := dc.ContainerCreate(ctx, config, hostConfig, nil, nil, "")
	if err != nil {
		return nil, fmt.Errorf("Unable to create Kubo container, Error: %w", err)
	}

	err = dc.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{})
	if err != nil {
		return nil, fmt.Errorf("Unable to start Kubo container, Error: %w", err)
	}

	kubo = &KuboNode{
		dc:          dc,
		containerID: resp.ID,
		swarmPort:   swarmPort,
		gatewayPort: gatewayPort,
		rpcAPIPort:  rpcAPIPort,
	}

	return kubo, nil
}

func (k *KuboNode) StopAndRemoveContainer(ctx context.Context) error {
	err := docker.StopAndRemoveContainerByID(ctx, k.dc, k.containerID)
	if err != nil {
		return err
	}
	zlog.Sugar().Debug("Kubo container stopped")
	return nil
}
