#!/bin/sh

set -ex
ipfs bootstrap rm all
ipfs config Swarm.DisableNatPortMap --bool true
ipfs config Discovery.MDNS.Enabled --bool false
ipfs config Addresses.API /ip4/127.0.0.1/tcp/31313
