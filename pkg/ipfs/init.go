package ipfs

import (
	"gitlab.com/nunet/data-persistence/internal/logger"
)

var zlog *logger.Logger

func init() {
	zlog = logger.NewZapLogger("ipfs")
}
