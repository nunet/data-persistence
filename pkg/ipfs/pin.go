package ipfs

import (
	"context"
	"fmt"

	"gitlab.com/nunet/data-persistence/pkg/docker"

	"github.com/ipfs/boxo/coreiface/options"
	"github.com/ipfs/boxo/coreiface/path"
	"github.com/ipfs/boxo/files"
	"github.com/ipfs/kubo/client/rpc"
)

func StoreOutput(ctx context.Context, containerID string) (string, error) {
	logs, err := docker.GetContainerLogs(ctx, containerID)
	if err != nil {
		zlog.Sugar().Errorf("couldn't get container logs, error: %v", err)
		return "", err
	}

	client, err := NewKuboClient()
	if err != nil {
		zlog.Sugar().Errorf("unable to connect to local Kubo node, error: %v", err)
		return "", err
	}

	cid, err := pinData(ctx, client, logs)
	if err != nil {
		zlog.Sugar().Errorf("couldnt' pin the output, error: %v", err)
		return "", err
	}
	zlog.Sugar().Infof("CID: %v", cid.String())

	return cid.String(), nil
}

func pinData(ctx context.Context, kubo *rpc.HttpApi, data []byte) (cid.Cid, error) {
	file := files.NewBytesFile(data)

	opt1 := options.Unixfs.Pin(true)
	opt2 := options.Unixfs.CidVersion(1)

	resolved, err := kubo.Unixfs().Add(ctx, file, opt1, opt2)
	if err != nil {
		zlog.Sugar().Errorf("Failed to do Unixfs().Add() operation: %v", err)
		return cid.Cid{}, err
	}

	return resolved.Root(), nil
}

func PinByCid(ctx context.Context, cid string) error {
	zlog.Sugar().Debugf("Pinning data based on CID %v", cid)
	client, err := NewKuboClient()
	if err != nil {
		return fmt.Errorf("Unable to connect to local Kubo node, error: %w", err)
	}

	err = pinByCid(ctx, client, cid)
	if err != nil {
		return err
	}

	return nil

}

func pinByCid(ctx context.Context, kubo *rpc.HttpApi, cid string) error {
	zlog.Sugar().Debug("Getting path from CID")
	p := path.New(cid)

	zlog.Sugar().Debug("Adding to Kubo")
	err := kubo.Pin().Add(ctx, p)
	if err != nil {
		return fmt.Errorf("Couldn't Pin data based on CID, Error: %w", err)
	}

	return nil
}
