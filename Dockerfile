# Start from golang base image with specific version
FROM golang:1.19 as builder

# Install protoc
RUN apt-get update && apt-get install -y protobuf-compiler

# Install the protoc plugins for Go
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Update or Init the git submodule for the .proto files
RUN git submodule update --init --remote api/ipfs_plugin/grpc/compute-api-spec && \
    cd api/ipfs_plugin/grpc/compute-api-spec && \
    git checkout v0.2.0

# Compile the .proto file
RUN sh scripts/compile_ipfs_plugin_proto.sh

# Build the Go app
RUN go build -o main ./cmd/ipfs_plugin/grpc_server.go

# Start a new stage from ubuntu
FROM ubuntu:20.04

WORKDIR /app

# Copy the Go executable from the builder stage
COPY --from=builder /app/main /app/main

# Expose port 31001
EXPOSE 31001

# Command to run the executable
CMD ["./main", "--addr", "0.0.0.0"]
